#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <signal.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>
#include "mialib_1.h"
#define USER_FILE "user.user"
#define NUSER 4
#define PORT 5001
#define IPSERVADDR "127.0.0.1"
#define MESSAGE_FILE "storageFile.dat"
/*Inizio Variabili Globali*/
sem_t semaphor = {0};
sem_t semlogin = {0};
int sockfd = 0;
Bulletin** arrayMes = {0};
user* users = {0};
/*Fine Variabili Globali*/
/*Inizio Prototipi Server*/
void init();
void startServer();
void* new_user_handler(void* arg);
void resetUser(int k);
int login(Message Msg, int* id);
void showMessage(thread_info* args, int sockfd);
int writeMessage(thread_info* args, Message msg);
int deleteMessage(thread_info* args, Message newMsg);
Bulletin** pullMessage();
void closer();
int numberOfMessage(int* m);
void closeConnection(thread_info* args);

/*Fine Prototipi Server*/

int main(int argc, char *argv[]) {
    signal(SIGINT, (void*) closer);
    signal(SIGHUP, SIG_IGN);
    signal(SIGQUIT, (void*) closer);
    signal(SIGTERM, (void*) closer);
    signal(SIGPIPE, SIG_IGN);
    startServer();
}

void init() {
    int fd = 0;
    int i = 0;
    users = malloc(NUSER * sizeof (user));
    user res = {0};
    fd = open(USER_FILE, O_RDONLY, 0660);
    if (fd == -1) {
        perror("Errore in apertura del file");
        exit(1);
    }
    while (read(fd, &res, sizeof (res)) > 0) {

        users[i] = res;

        i++;
    }
    close(fd);
}

void startServer() {

    int newsockfd, portno, clilen, ret;
    struct sockaddr_in serv_addr, cli_addr;
    int pshared = 0;
    pthread_t thread;
    arrayMes = pullMessage();


    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("ERRORE apertura socket\n");
        exit(1);
    }
    portno = PORT;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(IPSERVADDR);
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        perror("ERRORE di binding\n");
        exit(1);
    }
    init();
    if ((listen(sockfd, 5)) != 0) {
        printf("Ascolto fallito...\n");
        exit(0);
    } else {
        printf("Server in ascolto..\n");
        clilen = sizeof (cli_addr);
    }

    if (sem_init(&semaphor, pshared, 1) == -1) {
        printf("creazione semaforo MEM fallita\n");
        closer();
    } else {
        printf("creazione semaforo MEM OK\n");
    }
    if (sem_init(&semlogin, pshared, 1) == -1) {
        printf("creazione semaforo LOGIN fallita\n");
        closer();
    } else {
        printf("creazione semaforo LOGIN OK\n");
    }

    while (1) {
        printf("\n*************************startServer***************************\n");
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) {
            perror("Errore di connessione\n");
            exit(1);
        } else {
            printf("Connessione accettata\n");
        }
        thread_info *args = malloc(sizeof (thread_info));
        args->d = newsockfd;
        args->t = thread;
        if (pthread_create(&thread, NULL, new_user_handler, (void*) args) == 0) {
            printf("thread creato con successo\n");
        } else {
            close(args->d);
            free(args);
        }
    }
}

void* new_user_handler(void* arg) {
    thread_info* args = (thread_info*) arg;
    const unsigned short int N = 6;
    int *id = malloc(sizeof (int));
    *id = -1;
    args->id = id;
    Message newMsg = {0};
    int n = 0;
    int newsockfd = args->d;
    while (1) {
        bzero(&newMsg, sizeof (newMsg));
        n = read(args->d, (void *) &newMsg, 2 * sizeof (newMsg));
        if (n < 0) {
            printf("ERROR reading from socket di new_user_handler\n");
            resetUser(*id);
            closeConnection(args);
        }
        switch (newMsg.action) {
            case 1:
            { /*Login*/
                if (login(newMsg, id)) {
                    newMsg.result = 0;
                    n = write(newsockfd, &newMsg, sizeof (newMsg));
                    if (n < 0) {
                        printf("ERRORE scrittura su socket\n");
                        resetUser(*id);
                        closeConnection(args);
                    }
                } else {
                    printf("Client:%s non presente\n", newMsg.username);
                    newMsg.result = -1;
                    n = write(newsockfd, &newMsg, sizeof (newMsg));
                    if (n < 0) {
                        printf("ERRORE scrittura su socket\n");
                        resetUser(*id);
                        closeConnection(args);
                    }
                }
                break;
            }
            case 2:
            {/*Logout*/
                resetUser(*id);
                newMsg.result = 0;
                n = write(newsockfd, &newMsg, sizeof (newMsg));
                if (n < 0) {
                    printf("ERRORE scrittura su socket\n");
                    resetUser(*id);
                    closeConnection(args);
                }
                closeConnection(args);
                break;
            }
            case 3:
            {/*Salva Messaggio*/
                newMsg.idOwner = *id;
                Message risultato = {0};
                if (writeMessage(args, newMsg)) {
                    risultato.result = 0;
                    printf("Messaggio Salvato da SERVER");
                    n = write(newsockfd, &risultato, sizeof (risultato));
                    if (n < 0) {
                        printf("ERRORE scrittura su socket\n");
                        resetUser(*id);
                        closeConnection(args);
                    }
                } else {
                    risultato.result = -1;
                    n = write(newsockfd, &risultato, sizeof (risultato));
                    if (n < 0) {
                        printf("ERROR scrittura su socket\n");
                        resetUser(*id);
                        closeConnection(args);
                    }
                }

                break;
            }
            case 4:
            {/*Mostra Messaggi*/
                showMessage(args, newsockfd);
                break;
            }
            case 5:
            { /*Elimina Messaggio*/
                newMsg.idOwner = *id;
                if (deleteMessage(args, newMsg)) {
                    newMsg.result = 0;
                    n = write(newsockfd, &newMsg, sizeof (newMsg));
                    if (n < 0) {
                        printf("ERRORE scrittura su socket\n");
                        resetUser(*id);
                        closeConnection(args);
                    }
                } else {
                    newMsg.result = -1;
                    n = write(newsockfd, &newMsg, sizeof (newMsg));
                    if (n < 0) {
                        printf("ERRORE scrittura su socket\n");
                        resetUser(*id);
                        closeConnection(args);
                    }
                }
                break;
            }
            default:
            {
                newMsg.result = -1;
                n = write(newsockfd, (void*) &newMsg, sizeof (newMsg));
                if (n < 0) {
                    printf("ERRORE scrittura su socket\n");
                    resetUser(*id);
                    closeConnection(args);
                }
                resetUser(*id);
                closeConnection(args);
                break;
            }
        }

    }
}

void resetUser(int k) {
    sem_wait(&semlogin);
    int i = NUSER;
    while (i--) {
        if (k != users[i].id) {
            continue;
        } else {
            users[i].flag = 0;
            break;
        }
    }
    sem_post(&semlogin);
}

int login(Message Msg, int* id) {
    sem_wait(&semlogin);
    int n = 0;
    int n1 = 0;
    int i = NUSER;
    int flag = 0;
    while (i--) {
        n = strcmp(users[i].user, Msg.username);
        n1 = strcmp(users[i].pass, Msg.password);
        if ((n != 0) || (n1 != 0)) {
            continue;
        } else if ((n == 0) && (n1 == 0)) {
            if (users[i].flag == 0) {
                users[i].flag = 1;
                *id = users[i].id;
                flag = 1;
                break;
            }
        }
    }
    sem_post(&semlogin);
    return flag;
}

void showMessage(thread_info* args, int sockfd) {
    sem_wait(&semaphor);
    Message res = {0};
    bzero(&res, sizeof (res));    
    int ret = 0;
    int flag = 0;
    Bulletin* bull = *arrayMes;
    int n = bull->n; //#indice array di messaggi   
    if (n == 0) {
        res.result = -1;
        n = write(sockfd, &res, sizeof (res));
        if (n < 0) {
            printf("ERRORE scrittura su socket");
            closeConnection(args);
        }
    } else {
        res.bull = bull->m; //#messaggi !=-1
        res.result = 0;
        ret = write(sockfd, &res, sizeof (res));
        if (ret < 0) {
            printf("ERRORE lettura da socket\n");
            closeConnection(args);
        }
        ret = read(sockfd, &res, sizeof (res));        
        if (res.action == 0) {
            printf("Msg NON ricevuto da Client\n");
        } else {
            flag = 1;
            printf("Msg ricevuto da Client\n");
        }
        while ((n--)&& (flag == 1)) {
            res = bull->msg[n];
            if (res.idMessage != -1) {
                res.result = 0;
                ret = write(sockfd, &res, sizeof (res));
                if (ret < 0) {
                    printf("ERRORE lettura da socket\n");
                    closeConnection(args);
                }
                bzero(&res, sizeof (res));
                ret = read(sockfd, &res, sizeof (res));                
                if (ret < 0) {
                    printf("ERROR lettura da socket\n");
                    closeConnection(args);
                }
                if (res.action == 0) {
                    printf("Messaggio NON ricevuto da Client\n");
                    flag = 0;
                    break;
                } else {
                    printf("Messaggio ricevuto da Client\n");

                }
            }
        }
    }
    sem_post(&semaphor);

}

int writeMessage(thread_info* args, Message msg) {
    sem_wait(&semaphor);
    int x = 0;
    Bulletin** bulli = arrayMes;
    Bulletin* bull = *bulli;
    int n = bull->n;
    printf("Numero di Messaggi presenti nel Bull PRIMA DI SCRIVERE IL MESSAGGIO n°:%d\n", bull->m);
    bull->msg[n].idMessage = n;
    strncpy(bull->msg[n].object, msg.object, sizeof (msg.object));
    strncpy(bull->msg[n].sender, msg.sender, sizeof (msg.sender));
    strncpy(bull->msg[n].text, msg.text, sizeof (msg.text));
    strncpy(bull->msg[n].username, msg.username, sizeof (msg.username));
    bull->msg[n].idOwner = msg.idOwner;
    
    if ((n + 1)<(bull->size)) {
        bull->n++; //incremento #tot messaggi
        bull->m++; //incremento #msg != -1 
        x = 1;
        printf("Numero di Messaggi presenti nel Bull DOPO LA SCRITTURA n°:%d\n", bull->m);
    } else {        
        *bulli = realloc(bull, (bull->size)*2);
        bull->size = (bull->size)*2;        
    }
    sem_post(&semaphor);
    return x;
}

int deleteMessage(thread_info* args, Message msg) {
    sem_wait(&semaphor);
    Bulletin* bull = *arrayMes;
    int n = msg.idMessage;
    if (n >= 0 && n < bull->size) {
        if (bull->msg[n].idOwner == msg.idOwner) {
            bull->msg[n].idMessage = -1;
            bull->m--;
            sem_post(&semaphor);

            return 1;
        }
    }
    sem_post(&semaphor);
    return 0;
}

Bulletin** pullMessage() {
    int m = 0;
    int n = numberOfMessage(&m);
    Message* msg = {0};
    int size;
    Bulletin** bulli = malloc(sizeof (Bulletin*));
    Bulletin* bull = malloc(sizeof (Bulletin));
    *bulli = bull;
    printf("Messaggi Tot nel Bull n: %d Messaggi validi m: %d\n", n, m);
    if (n != 0) {
        size = n + 100;
    } else {
        size = 1000;
    }
    msg = (Message*) malloc(size * sizeof (Message));
    Message mread = {0};
    int k = 0;
    int fd = open(MESSAGE_FILE, O_RDONLY | O_CREAT, 0660);
    if (fd == -1) {
        printf("Apertura MESSAGE_FILE Fallita\n");
        bull->msg = msg;
        bull->m = m;
        bull->n = n;
        bull->size = size;
        return bulli;
    } else {
        for (int i = 0; i < n && (read(fd, &mread, sizeof (mread)) > 0); i++) {
            msg[i] = mread;
        }
        close(fd);
    }

    bull->msg = msg;
    bull->m = m;
    bull->n = n;
    bull->size = size;
    return bulli;
}

void closer() {
    sem_wait(&semaphor);
    sem_wait(&semlogin);
    Message msg = {0};
    int size = sizeof (msg);
    Bulletin* bull = *arrayMes;
    int n = bull->n;
    int cd = open(MESSAGE_FILE, O_WRONLY, 0333);
    int k = 0;
    while (n--) {
        if (k >= 0 && k < bull->size) {
            msg = bull->msg[k];
            int nwrite = write(cd, &msg, size);
            if (nwrite > 0) {
                printf("\nScrittura msg:%d su file OK\n",k);
            } else {
                printf("\nScrittura msg:%d su file KO\n",k);
            }

        } else {
            printf("\nNumero Msg fuori memoria\n");
        }
        k++;
    }
    close(cd);
    free(arrayMes);
    close(sockfd);
    sem_post(&semaphor);
    sem_post(&semlogin);
    exit(0);
}

int numberOfMessage(int* m) {
    Message msg = {0};
    int n = 0;
    *m = 0;
    int fd = open(MESSAGE_FILE, O_RDONLY, 0660);
    if (fd == -1) {
        *m = 0;
        printf("\nMessage_file non aperto n: %d m: %d\n", n, *m);
        return n;
    }

    int k = 0;
    while (read(fd, &msg, sizeof (msg)) > 0) {
        if (msg.idMessage != -1)*m = *m + 1;
        n++; //messaggi totali
    }
    close(fd);
    return n;
}

void closeConnection(thread_info* args) {
    pthread_t idt = args->t;
    free(args->id);
    free(args);
    close(args->d);
    pthread_exit((void*) idt);
}
