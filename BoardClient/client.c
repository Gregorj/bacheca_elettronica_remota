#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include "mialib_1.h"

#define PORT 5001
#define HOSTNAME "localhost"

/*Inizio Variabili Globali*/
int sockfd;
/*Fine Variabili Globali*/


/*Inizio Prototipi Client*/
int login(char* user, char* psw);
int logout(Message msg);
int sendMessage(Message msg);
void showMessage(Message msg);
void deleteMessage(Message msg);
int take_string(char* ip, int dim);
int take_int();
void closeConnection();

/*Fine Prototipi Client*/

int main(int argc, char *argv[]) {
    signal(SIGPIPE, (void*) closeConnection);
    signal(SIGINT, (void*) closeConnection);
    signal(SIGHUP, SIG_IGN);
    signal(SIGQUIT, (void*) closeConnection);
    signal(SIGTERM, (void*) closeConnection);
    
    int portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char username[6], password[6];

    portno = PORT;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("ERRORE apertura socket");
        exit(1);
    }
    server = gethostbyname(HOSTNAME);
    if (server == NULL) {
        fprintf(stderr, "ERRORE, nome host\n");
        exit(0);
    }
    ((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr)) < 0) {
        perror("ERRORE connessione");
        exit(1);
    }

    int q = 0;
    int k = 0;
    int i = 0;
    int nMsg = 0;
    Message msg = {0};
    while (1) {
        if (q == 0) {
            printf("Per entrare inserisci credenziali\nuser1\npass1\nuser2\npass2\nuser3\npass3\nuser4\npass4\n");
            printf("Username: \n");
            take_string(username, sizeof (username));

            printf("Password: \n");
            take_string(password, sizeof (password));
            if (login(username, password)) {
                printf("\nLogin avvenuto con successo\n");
                q = 1;

                strncpy(msg.username, username, sizeof (msg.username));
                strncpy(msg.password, password, sizeof (msg.password));
                break;
            } else {
                printf("\nLogin fallito q:%d\n", q);
            }
        }
    }
    while (q == 1) {
        printf("Ciclo WHILE n°: %d\n - sockfd: %d\n", k, sockfd);
        k++;
        printf("1.  Invia messaggio\n");
        printf("2.  Elimina messaggio\n");
        printf("3.  Logout\n");
        printf("4.  Visualizza messagggi in Bacheca\n");
        i = take_int();
        switch (i) {
            case 1:
                sendMessage(msg);
                break;
            case 2:
                deleteMessage(msg);
                break;
            case 3:
                if (logout(msg)) {
                    printf("Logout avvenuto con successo\n");
                    close(sockfd);
                    exit(EXIT_SUCCESS);

                } else {
                    printf("Logout Errore\n");
                }
                break;
            case 4:
                showMessage(msg);
                break;
            default:
                printf("Non hai digitato un carattere compreso fra 1 ed 3\n");
                break;
        }
    }
}

int login(char* user, char* psw) {
    int n;
    Message newMsg = {0};
    strncpy(newMsg.username, user, sizeof (user));
    strncpy(newMsg.password, psw, sizeof (psw));
    newMsg.action = 1;
    n = write(sockfd, (void *) &newMsg, sizeof (newMsg));
    if (n < 0) {
        closeConnection();
    }
    n = read(sockfd, (void *) &newMsg, sizeof (newMsg));
    if (n < 0) {
        closeConnection();
    }
    if (newMsg.result == 0) {

        return 1;
    } else {
        return 0;
    }
}

int logout(Message msg) {
    int n;

    msg.action = 2;
    n = write(sockfd, (void *) &msg, sizeof (msg));
    if (n < 0) {
        closeConnection();
    }


    n = read(sockfd, (void *) &msg, sizeof (msg));


    if (n < 0) {
        closeConnection();
    }
    if (msg.result == 0) {
        return 1;
    } else {
        return 0;
    }
}

int sendMessage(Message msg) {
    int n;
    msg.action = 3;

    printf("Mittente: \n");
    take_string(msg.sender, sizeof (msg.sender));

    printf("Oggetto: \n");
    take_string(msg.object, sizeof (msg.object));

    printf("Testo: \n");
    take_string(msg.text, sizeof (msg.text));


    n = write(sockfd, (void *) &msg, sizeof (msg));
    if (n < 0) {
        closeConnection();
    }


    n = read(sockfd, (void *) &msg, sizeof (msg));
    if (n < 0) {
        closeConnection();
    }
    if (msg.result == 0) {
        printf("Il server  ha salvato\n");
        return 1;
    } else {
        printf("Il server non ha salvato result!=0\n");
        return 0;
    }
}

int take_string(char* ip, int dim) {
    int num;
    while (1) {
        char* line = NULL;
        size_t n = 0;
        num = getline(&line, &n, stdin);
        if (num <= dim && num > 1) {
            strncpy(ip, line, dim);
            num--;
            ip[num] = '\0';
            break;
        } else {
            printf("Hai inserito una stringa troppo grande: Inserire una stringa di massimo %d caratteri\n", dim - 1);
        }
    }
    return 0;
}

int take_int() {
    int n = -1;
    while (1) {
        char* p;
        char* buff = NULL;
        size_t num = 0;
        if (getline(&buff, &num, stdin) > 0) {
            n = strtoull(buff, &p, 10);
            if (*p == '\n' || *p == '\0') break;
        } else {
            printf("Input non cosiderato corretto:riprovare\n");
        }
    }
    return n;
}

void showMessage(Message msg) {
    int n = 0;
    int m = 0;
    Message resp = {0};
    msg.action = 4;
    n = write(sockfd, (void *) &msg, sizeof (msg));
    if (n < 0) {
        closeConnection();
    }
    bzero(&resp, sizeof (resp));
    n = read(sockfd, (void *) &resp, sizeof (resp));
    if (n < 0) {
        closeConnection();
    }
    if (resp.result == 0) {
        m = resp.bull;
        if (m != 0) {
            msg.action = 1; //1 ho ricevuto il messaggio precedente sono pronto a ricevere il successivo 
            n = write(sockfd, (void *) &msg, sizeof (msg));
            if (n < 0) {
                closeConnection();
            }
            printf("\n n°: %d Messaggi in Bacheca\n", m);
            while (m--) {

                bzero(&resp, sizeof (resp));
                n = read(sockfd, (void *) &resp, sizeof (resp));
                if (n < 0) {
                    closeConnection();
                }
                msg.action = 1;
                n = write(sockfd, (void *) &msg, sizeof (msg));
                if (n < 0) {
                    closeConnection();
                }
                if (resp.result == 0) {
                    printf("****************************************\n");
                    printf("id: %d\n", resp.idMessage);
                    printf("Mittente: %s\n", resp.sender);
                    printf("Oggetto: %s\n", resp.object);
                    printf("Testo: %s\n", resp.text);
                }
            }
        }
    } else {
        printf("%s\n", "Nessun Messaggio in Bacheca\n");
        msg.action = 0;
        n = write(sockfd, (void *) &msg, sizeof (msg));
        if (n < 0) {
            closeConnection();
        }
    }
    printf("****************************************\n");
}

void deleteMessage(Message msg) {
    printf("Inserisci ID messaggio da eliminare\n");
    int k = take_int();
    msg.idMessage = k;
    int n;
    msg.action = 5;
    printf("Il messaggio che si è scelto di eliminare è Messaggio n°: %d\n", msg.idMessage);
    n = write(sockfd, (void *) &msg, sizeof (msg));
    if (n < 0) {
        closeConnection();
    }
    n = read(sockfd, (void *) &msg, sizeof (msg));
    if (n < 0) {
        closeConnection();
    }
    if (msg.result == 0) {
        printf("Messaggio n° %d eliminato con successo\n", k);
    } else {
        printf("Non sei autorizzato ad eliminare il messaggio n° %d\n", k);
    }

}

void closeConnection() {
    printf("\nLa connessione con il server è stata chiusa!\n");
    close(sockfd);
    exit(0);
}
