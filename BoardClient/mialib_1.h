#ifndef MIALIB_H
#define MIALIB_H


typedef struct {
    int action;
    char username[6];
    char password[6];
    int result;   
    int idOwner;
    int idMessage;
    char sender[15];
    char object[30];
    char text[600];
    int bull;
} Message;

typedef struct {
    int id;
    char user[6];
    char pass[6];
    int flag;
} user;

typedef struct {
    Message* msg;
    int n;
    int m;
    int size;
} Bulletin;

typedef struct {
    int d;
    int* id;
    Message m;
    pthread_t t;
} thread_info;
#endif /* MIALIB_H */

